﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesSystem
{
    public class Sale
    {
        public Sale(int id, double value)
        {
            this.id = id;
            this.value = value;
        }

        public Sale(int id, double value, int month, int year)
        {
            this.id = id;
            this.value = value;
            this.month = month;
            this.year = year;
        }

        public int id { get; set; }
        public double value { get; set; }
        public int month { get; set; }
        public int year { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SalesSystem
{
    public class SalesRepository
    {
        public virtual List<Sale> GetSales(int month, int year)
        {
            List<Sale> sales = new List<Sale>();

            //please, pretend that I'm usign a real DB, ok? ;)
            List<Sale> saleData = new List<Sale>();
            saleData.Add(new Sale(1, 500, 4, 2019));
            saleData.Add(new Sale(2, 500, 3, 2019));
            saleData.Add(new Sale(3, 20000, 3, 2019));
            saleData.Add(new Sale(4, 500.47, 2, 2019));

            foreach (Sale sale in saleData)
            {
                if (sale.month == month && sale.year == year)
                {
                    sales.Add(sale);
                }
            }

            return sales;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesSystem
{
    public class ComissionCalculator
    {
        public virtual double Calculate(double saleValue)
        {
            var comission = 0.0;
            if (saleValue < 10000)
            {
                comission = saleValue * 0.05;
            }
            else
            {
                comission = saleValue * 0.06;
            }

            return Math.Floor(comission * 100)/ 100;
        }
    }
}

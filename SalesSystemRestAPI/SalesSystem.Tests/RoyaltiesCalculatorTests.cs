using Microsoft.VisualStudio.TestTools.UnitTesting;
using SalesSystem;
using Moq;
using System.Collections.Generic;

namespace SalesSystem.Tests
{
    [TestClass]
    public class RoyaltiesCalculatorTests
    {
        Mock<SalesRepository> mockedRepository;
        List<Sale> fakeSalesList;

        [TestInitialize]
        public void mockSetUp()
        {
            mockedRepository = new Mock<SalesRepository>();
            fakeSalesList = new List<Sale>();
        }

        [TestMethod]
        public void TestJan2030WithoutSalesReturn0USDRoyalties()
        {
            int month = 5;
            int year = 2019;
            double expectedRoaylaties = 0;

            var royaltiesCalculator = new RoyaltiesCalculator(mockedRepository.Object);

            double actualRoyalties = royaltiesCalculator.Calculate(month, year);

            Assert.AreEqual(expectedRoaylaties, actualRoyalties);
        }

        [TestMethod]
        public void TestApril2019WithOneSaleOf500Return95USDRoyalties()
        {
            int month = 04;
            int year = 2019;
            double saleValue = 500;
            double expectedRoaylaties = 95;         

            fakeSalesList.Add(new Sale(1, saleValue));

            mockedRepository.Setup(r => r.GetSales(month, year)).Returns(fakeSalesList);

            var royaltiesCalculator = new RoyaltiesCalculator(mockedRepository.Object);

            double actualRoyalties = royaltiesCalculator.Calculate(month, year);

            Assert.AreEqual(expectedRoaylaties, actualRoyalties);
        }

        [TestMethod]
        public void TestMarch2019WithTwoSaleOf500And20000Return3855USDRoyalties()
        {
            int month = 03;
            int year = 2019;
            double firstSaleValue = 500;
            double secondSaleValue = 20000;
            double expectedRoaylaties = 3855;

            var mockedRespository = new Mock<SalesRepository>();
            var fakeSalesList = new List<Sale>();

            fakeSalesList.Add(new Sale(1, firstSaleValue));
            fakeSalesList.Add(new Sale(2, secondSaleValue));

            mockedRespository.Setup(r => r.GetSales(month, year)).Returns(fakeSalesList);

            var royaltiesCalculator = new RoyaltiesCalculator(mockedRespository.Object);

            double actualRoyalties = royaltiesCalculator.Calculate(month, year);

            Assert.AreEqual(expectedRoaylaties, actualRoyalties);
        }

        [TestMethod]
        public void TestFeb2019WithOneSaleOf500_47Return95_09USDRoyalties()
        {
            int month = 02;
            int year = 2019;
            double saleValue = 500.47;
            double expectedRoaylaties = 95.09;

            fakeSalesList.Add(new Sale(1, saleValue));

            mockedRepository.Setup(r => r.GetSales(month, year)).Returns(fakeSalesList);

            var royaltiesCalculator = new RoyaltiesCalculator(mockedRepository.Object);

            double actualRoyalties = royaltiesCalculator.Calculate(month, year);

            Assert.AreEqual(expectedRoaylaties, actualRoyalties);
        }
    }
}

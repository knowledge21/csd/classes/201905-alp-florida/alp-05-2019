using Microsoft.VisualStudio.TestTools.UnitTesting;
using SalesSystem;
using Moq;
using System.Collections.Generic;

namespace SalesSystem.Tests
{
    [TestClass]
    public class ComissionCalculatorTests
    {
        [TestMethod]
        public void TestSaleOf500_47ReturnsComissionOf25_02()
        {
            double saleValue = 500.47;
            double expectedComission = 25.02;

            var comissionCalculator = new ComissionCalculator();

            double actualComission = comissionCalculator.Calculate(saleValue);

            Assert.AreEqual(expectedComission, actualComission);
        }
    }
}

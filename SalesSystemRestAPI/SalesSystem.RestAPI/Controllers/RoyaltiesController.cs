﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace SalesSystem.RestAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoyaltiesController : ControllerBase
    {
        // GET api/values/5
        [HttpGet("{month}/{year}")]
        public ActionResult<double> Get(int month, int year)
        {
            return new RoyaltiesCalculator().Calculate(month, year);
        }
    }
}
